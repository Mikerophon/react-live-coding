get-dependencies:
	npm install -d --registry=http://registry.npmjs.org

# setup all needed tools
setup: install-nodejs install-npm

install-nodejs:
	curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
	sudo apt-get install -y nodejs

install-npm:
	sudo apt-get install npm
	npm install -g npm@latest

# starting the application locally
run: get-dependencies
	npm run start
