/**
 * Configuration loaded by config-loader.
 *
 * Usage:
 *   var config = require('webpack-config-loader!../app-config.js');
 *   config.advertiserBillingBaseUrl
 */
module.exports = {
  development: {
    userServiceEndpoint: 'http://localhost:4000/users'
  }
};
