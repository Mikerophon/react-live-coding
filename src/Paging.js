import React, {Component} from 'react';

export default class Paging extends Component {

  constructor(props) {
    super(props);

    this.jumpToPage = this.jumpToPage.bind(this);
  }

  render() {
    return (
      <div>
        {!this.props.metadata.isFirstPage &&
        <button onClick={this.jumpToPage(this.props.metadata.currentPage -1)}>Previous</button>
        }
        {!this.props.metadata.isLastPage &&
        <button onClick={this.jumpToPage(this.props.metadata.currentPage +1)}>Next</button>
        }
      </div>
    )
  }

  jumpToPage(page) {
    return function (e) {
      e.preventDefault();
      this.props.pagingCallback(page);
    }.bind(this)
  }
}