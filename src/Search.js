import React, {Component} from 'react'

export default class Search extends Component {

  constructor(props) {
    super(props)

    this.newSearch = this.newSearch.bind(this);
  }

  render() {
    return (
      <div>
        Search:
        <input defaultValue={this.props.searchString} onChange={this.newSearch} />
      </div>
    )
  }

  newSearch(event) {
    this.props.searchCallback(event.target.value);
  }
}