import React, {Component} from 'react';
import Paging from './Paging';

export default class Table extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    let tableData = (!this.props.data ? [] : this.props.data.map(this.dataMapper));
    return (
      <div>
        <table>
          <thead>
          <tr>
            <th>Name</th>
            <th>Country</th>
          </tr>
          </thead>
          <tbody>{tableData}</tbody>
        </table>
        <Paging metadata={this.props.metadata} pagingCallback={this.props.pagingCallback}/>
      </div>)
  }

  dataMapper(user, index) {
    return (
      <tr key={user.userId}>
        <td id={`name${user.userId}`}>{user.name}</td>
        <td id={`country${user.userId}`}>{user.country}</td>
      </tr>
    )
  }
}

