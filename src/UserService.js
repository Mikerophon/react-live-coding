import zepto from 'npm-zepto';

export default class userService {

  constructor(props) {
    this.props = props;
  }

  getUsers(query, page, pageSize, callbackSucess, callBackError) {
    $.ajax({
      headers: {'Content-Type': 'text/plain'
      },
      type: "GET",
      url: this.props.baseUrl + "?page=" + page + "&pageSize=" + pageSize + "&query=" + query,
      dataType: 'json',
      success: function (data) {
        callbackSucess(data);
      },
      error: function (data) {
        callBackError(data);
      }
    });
  }

}
