import React, {Component} from 'react';
import Table from './Table';
import Search from './Search'

export default class App extends Component {

  constructor(props) {
    super(props);

    this.loadUsersForPageAndQuery = this.loadUsersForPageAndQuery.bind(this);
    this.loadUsersForPage = this.loadUsersForPage.bind(this);
    this.loadUsersForQuery = this.loadUsersForQuery.bind(this);

    this.state = {
      users: [],
      usersMeta: {
        currentPage: 0,
        isFirstPage: true,
        totalNumberOfPages: 0,
        isLastPage: true
      },
      searchString: ""
    }
  }

  render() {
    return (
      <div>
        Hello {this.props.name}!
        <Search searchString={this.state.searchString} searchCallback={this.loadUsersForQuery}/>
        <Table data={this.state.users} metadata={this.state.usersMeta} pagingCallback={this.loadUsersForPage}/>
      </div>
    )
  }

  componentDidMount() {
    this.loadUsersForPageAndQuery(0, this.state.searchString);
  }

  loadUsersForQuery(query) {
    this.setState({searchString: query});
    this.loadUsersForPageAndQuery(0, query);
  }

  loadUsersForPage(page) {
    this.loadUsersForPageAndQuery(page, this.state.searchString);
  }

  loadUsersForPageAndQuery(page, query) {
    var thisComponent = this;
    this.props.userService.getUsers(
      query, page, 5,
      function (data) {
        thisComponent.setState({users: data.users})
        thisComponent.setState({usersMeta: {
          currentPage: data.page,
          isFirstPage: data.firstPage,
          totalNumberOfPages: data.totalPages,
          isLastPage: data.lastPage
        }});
      },
      function (data) {
        console.log("Failed")
      }
    )
  }
}
