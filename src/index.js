import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import appConfig from 'webpack-config-loader!../app-config.js';
import UserService from './UserService.js';

var userService = new UserService({baseUrl: appConfig.userServiceEndpoint});

ReactDOM.render(
  <App name="AWIN" userService={userService}/>,
  document.getElementById("usersOverviewContainer"));